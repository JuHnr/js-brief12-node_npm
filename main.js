import validator from 'validator'; 

//tableau d'email à valider
const emails = [
    'john.doe@example.com',
    'alice.smith@gmail.com',
    'bob.jones123@company.org',
    'invalid.email@',
    'missingatdomain.com',
    'user@invaliddomain',
    'spaces in@email.com',
    '@missingusername.com'
];

//fonction pour valider les emails grâce à la méthode du validator isEmail
const isEmailValid = (email) => validator.isEmail(email);


//itère dans le tableau et vérifie si l'email est valide grâce à la fonction précédente
for (const eachMail of emails) {
    console.log(eachMail, isEmailValid(eachMail)); //affiche le résultat 
}


